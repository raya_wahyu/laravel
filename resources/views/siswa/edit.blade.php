<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="body bg-dark">

<nav class="navbar navbar-dark bg-succses" style="border-bottom:1px solid white;">
  <a class="navbar-brand" href="/home_web">
    <img src="/docs/4.4/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
   <div class="btn bg-dark text-light rounded-pill" style="border: double white; "> Rexensoft</div>
  </a>
</nav>

<div class="container">
	<h1 class="text-light">Edit Data Siswa X-RPL</h1>
	<div class="row " style="border: 4px double white; float: center;">
		<div class="col-lg-12">
		 <form action="/siswa/{{$siswa->id}}/update" method="POST">
				       		{{csrf_field()}}
				       			<!-- FORM -->
							  
							    

							      <label for="inputEmail4" style="color: white; float: center;">Nama Depan</label>
							      <input name="nama_depan" type="text" class="form-control bg-dark text-danger" id="inputEmail4" placeholder="Nama Depan" value="{{$siswa->nama_depan}}">
							    
							    
							      <label for="inputPassword4" style="color: white;">Nama Belakang</label>
							      <input name="nama_belakang" type="text" class="form-control bg-dark text-danger" id="inputPassword4" placeholder="Nama Belakang" value="{{$siswa->nama_belakang}}">
							    
							  
							  
							    <label for="inputAddress" style="color: white;">Agama</label>
							    <input name="agama" type="text" class="form-control bg-dark text-danger" id="inputAddress" placeholder="Agama" value="{{$siswa->agama}}">
							 
							 
							 
							     
								    <label for="exampleFormControlTextarea1" style="color:white;">Alamat</label>
								    <textarea name="alamat" class="form-control bg-dark text-danger" id="exampleFormControlTextarea1" rows="3" >{{$siswa->alamat}}</textarea>
								 
							   
							      <label for="inputState" style="color: white;">Jenis Kelamin</label>
							      <select name="jenis_kelamin" id="inputState" class="form-control bg-dark text-danger">
							        <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>Laki-Laki</option>
							        <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
							      </select>

							      
								      <label for="inputZip" style="color: white;">Absen</label>
								      <input name="absen" type="number" min="1" max="36" class="form-control bg-dark text-danger" id="inputZip" value="{{$siswa->absen}}">
								    
							   
							    
							  
							  <div class="form-group bg-dark text-danger">
							    
							  </div>
							  <button type="submit" class="btn btn-outline-warning">Update</button>
							  <a href="/siswa" class="btn btn-outline-light float-right">Keluar</a>
							</form>
							</div>
		
	</div>
</div>
	

				


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

