<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!-- Btsp -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Favicons -->
<link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
<link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
<link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
<meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#563d7c">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @font-face {
        font-family: keren;
        src: url(..\xampp\htdocs\font.ttf);
      }


      

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>
<body class="body bg-dark">
   <header>
  <nav class="navbar navbar-dark bg-dark" >
  <a class="navbar-brand" href="/home_web">
    <svg style="color: red;" class="bi bi-house-door " width="6%" height="6%" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M7.646 1.146a.5.5 0 01.708 0l6 6a.5.5 0 01.146.354v7a.5.5 0 01-.5.5H9.5a.5.5 0 01-.5-.5v-4H7v4a.5.5 0 01-.5.5H2a.5.5 0 01-.5-.5v-7a.5.5 0 01.146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 01.5-.5h3a.5.5 0 01.5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" clip-rule="evenodd"/>
        <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
     </svg>
   <div class="btn bg-dark text-danger rounded-pill" style="border: double red; "> Rexensoft</div>
  </a>
    <div class="col-8 text-danger" > 
      <h1 style="margin-left: 75px;"> WELCOME TO REXENSOFT</h1>
    </div>
</nav>
</header>

<main role="main">

  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="border-bottom: 1px solid red;">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <svg class="bd-placeholder-img " width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#343a40"/></svg>
        <div class="container">
          <div class="carousel-caption text-left bg-dark">
            <h1>List</h1>
            <p>this is a website that I created to provide information and data for 10th graders of software engineering.</p>
            <p><a class="btn btn-lg btn-outline-danger" href="/siswa" role="button">Look it up</a></p>
          </div>
        </div>
      </div>
      <div class="carousel-item">
        <svg class="bd-placeholder-img" width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="#343a40"/></svg>
        <div class="container">
          <div class="carousel-caption text-right">
            <h1>Edit</h1>
            <p>Did you register then you have data that is wrong? You can edit your data here but you must register before, come on.</p>
            <p><a class="btn btn-lg btn-outline-warning" href="/siswa" role="button">Edit</a></p>
          </div>
        </div>
      </div>
      
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

<div class="row">
  
<svg class="bd-placeholder-img rounded-circle col-md-5" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><title>Placeholder</title><rect width="100%" height="100%" fill="#343a40"/><text x="50%" y="50%" fill="#343a40" dy=".3em">140x140</text></svg>

<svg version="1.0" xmlns="http://www.w3.org/2000/svg"
 width="27%" height="27%" viewBox="0 0 1280.000000 1280.000000"
 preserveAspectRatio="xMidYMid meet" class="col-2 rounded-circle">

<g transform="translate(0.000000,1280.000000) scale(0.100000,-0.100000)"
fill="#bf0000" stroke="none" >
<path d="M6105 11113 c-22 -2 -103 -10 -180 -19 -1228 -134 -2376 -764 -3153
-1730 -89 -111 -252 -334 -252 -346 0 -5 524 -8 1165 -8 929 0 1165 -3 1165
-13 0 -6 119 -131 264 -276 l264 -263 117 91 c64 51 119 91 121 89 2 -2 18
-112 35 -243 l33 -240 200 -202 200 -203 1828 0 1828 0 2 269 3 270 135 -77
c610 -349 979 -566 973 -573 -4 -4 -174 -103 -378 -220 -203 -117 -450 -259
-549 -316 -98 -56 -180 -103 -182 -103 -2 0 -4 97 -4 215 l0 215 -1892 0
-1893 0 -280 279 -280 279 -190 77 c-104 42 -196 80 -203 84 -12 7 36 50 113
103 6 4 -79 97 -205 223 l-215 215 -1190 0 -1190 0 -69 -137 c-231 -460 -366
-874 -460 -1400 l-6 -33 3030 0 3030 0 0 -155 0 -155 -3099 0 c-1705 0 -3102
-3 -3104 -7 -3 -5 -8 -65 -12 -135 l-7 -128 55 0 54 0 6 -252 c9 -403 58 -748
158 -1120 l10 -38 946 0 946 0 171 99 c94 55 179 103 189 107 15 6 17 -3 17
-100 l0 -106 263 0 262 0 630 630 630 630 1170 0 1171 0 204 119 205 118 3
-118 3 -119 1234 0 1235 0 0 76 c0 101 -17 339 -36 497 -94 805 -403 1586
-886 2238 -268 362 -614 712 -973 981 -700 525 -1522 840 -2410 923 -145 13
-648 19 -770 8z m-3455 -3103 l0 -160 -155 0 -155 0 0 160 0 160 155 0 155 0
0 -160z"/>
<path d="M5467 5452 c-339 -339 -617 -623 -617 -630 0 -9 -75 -12 -340 -12
l-340 0 0 -130 c0 -71 -3 -130 -6 -130 -3 0 -107 59 -230 130 l-225 130 -853
-2 -854 -3 59 -150 c112 -281 299 -637 469 -890 329 -491 795 -958 1280 -1282
602 -402 1251 -654 1955 -758 420 -62 959 -61 1385 1 1295 190 2464 924 3212
2019 429 627 709 1386 787 2135 6 58 13 124 16 148 l5 42 -1230 0 -1229 0 -3
-119 -3 -120 -205 119 -205 119 -1105 1 -1105 0 -618 -618z"/>
</g>
</svg>

</div>
<div class="text">
  <h2 style="font-family: 'keren'; text-align: center; color: red; font-variant: inherit;"> ReXensoft</h2>
</div>










<!-- JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>