<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body class="body bg-dark">

<nav class="navbar navbar-dark bg-dark" style="border-bottom:1px solid red;">
  <a class="navbar-brand" href="/home_web">
    <img src="/docs/4.4/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
   <div class="btn bg-dark text-danger rounded-pill" style="border: double red; "> Rexensoft</div>
  </a>
		<div class="col-6">
			<button type="button" class="btn btn-outline-danger " data-toggle="modal" data-target="#exampleModal" style="float: right;">
				  Tambah
				</button>
		</div>
</nav>

<div class="container">
	

	<div class="row">
			<div class="col-6">
			<h1 style="color: red;">Data Siswa Kelas X-RPL</h1>
		</div>
		
		
				<table class="table table-dark table-bordered border-dark">
					<tr>
						<th>Absen</th>
						<th>Nama Depan</th>
						<th>Nama Belakang</th>
						<th>Jenis Kelamin</th>
						<th>Agama</th>
						<th>Alamat</th>
						<th>Aksi</th>
					</tr>
					@foreach($data_siswa as $siswa)
					<tr>
						<td>{{$siswa->absen}}</td>
						<td>{{$siswa->nama_depan}}</td>
						<td>{{$siswa->nama_belakang}}</td>
						<td>{{$siswa->jenis_kelamin}}</td>
						<td>{{$siswa->agama}}</td>
						<td>{{$siswa->alamat}}</td>
						<td>
							<a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm">edit</a>
							<a href="/siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Hapus?')">delete</a>

						</td>
					</tr>
					@endforeach
				</table>
	</div>
</div>
	

				<!-- Modal -->
				<div class="modal fade bg-dark text-danger" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header bg-dark text-danger">
				        <h5 class="modal-title bg-dark text-danger" id="exampleModalLabel">Tambah Data</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body bg-dark text-danger">
				       <form action="/siswa/create" method="POST">
				       		{{csrf_field()}}
				       			<!-- FORM -->
							  <div class="form-row ">
							    <div class="form-group col-md-6">

							      <label for="inputEmail4">Nama Depan</label>
							      <input name="nama_depan" type="text" class="form-control bg-dark text-danger" id="inputEmail4" placeholder="Nama Depan">
							    </div>
							    <div class="form-group col-md-6">
							      <label for="inputPassword4">Nama Belakang</label>
							      <input name="nama_belakang" type="text" class="form-control bg-dark text-danger" id="inputPassword4" placeholder="Nama Belakang">
							    </div>
							  </div>
							  <div class="form-group">
							    <label for="inputAddress">Agama</label>
							    <input name="agama" type="text" class="form-control bg-dark text-danger" id="inputAddress" placeholder="Agama">
							  </div>
							 
							  <div class="form-row">
							     <div class="form-group">
								    <label for="exampleFormControlTextarea1">Alamat</label>
								    <textarea name="alamat" class="form-control bg-dark text-danger" id="exampleFormControlTextarea1" rows="3"></textarea>
								  </div>
							    <div class="form-group col-md-4">
							      <label for="inputState">Jenis Kelamin</label>
							      <select name="jenis_kelamin" id="inputState" class="form-control bg-dark text-danger">
							        <option value="L">Laki-Laki</option>
							        <option value="P">Perempuan</option>
							      </select>

							      
								      <label for="inputZip" >Absen</label>
								      <input name="absen" type="number" min="1" max="36" class="form-control bg-dark text-danger" id="inputZip">
								    
							    </div>
							    
							  </div>
							  <div class="form-group bg-dark text-danger">
							    
							  </div>
							  <button type="submit" class="btn btn-outline-danger">Tambah</button>
							</form>
				      </div>
				      <div class="modal-footer bg-dark text-danger">
				        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Tutup</button>
				        <button type="button" class="btn btn-outline-danger">Simpan Perubahan</button>
				      </div>
				    </div>
				  </div>
				</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

